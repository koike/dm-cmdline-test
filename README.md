dm-cmdline-test
---------------

Script to test creating mapped devices from command line parameter

Pre-requirements
----------------

* kvm
* qemu
* sgdisk
* dmsetup
* blockdev
* veritysetup
* 6G of available space

Usage
-----

    git clone https://gitlab.collabora.com/koike/dm-cmdline-test.git
    cd dm-cmdline-test
    ./dm-cmdline-test ${KERNEL_PATH}/arch/x86_64/boot/bzImage

How the test works
------------------

The script creates several virtual disks in different formats with a minimal
rootfs containing just a simple busybox and init script.

The init script prints a message and shutdowns the machine.

The test launches several kvm/qemu machines with the created disks and
different arguments for "dm-init.create=". If the machine stops within a
certain amount of time, it means the machine booted and executed the init
scripts successfully, otherwise it failed.
