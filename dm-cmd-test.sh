#!/bin/bash

# shellcheck disable=SC2154
# shellcheck disable=SC1090

set -eo pipefail

if [ "$#" -ne 1 ]; then
    echo "Illegal number of parameters"
    echo "Usage: $0 <path-to-kernel-bzimage>"
    exit 1
fi
KERNEL=$1

finish() {
	ret=$?
	if [ "$ret" != "0" ]; then
		echo ""
		echo "Script couldn't be executed."
		echo "You might need to do the following cleanups by hand:"
		echo "	sudo losetup"
		echo "	sudo losetup -d /dev/loopX"
		echo "	sudo dmsetup ls"
		echo "	sudo dmsetup remove dm-XXXXXX"
	fi

	exit $ret
}
trap "finish" INT TERM EXIT

fill_rootfs()
{
	MSG=${1-""}
	# Add busybox and init script
	if [ ! -f busybox ]; then
		wget https://busybox.net/downloads/binaries/1.21.1/busybox-x86_64 -O busybox
		chmod +x busybox
	fi
	sudo mkdir dm-mount/bin
	sudo cp busybox dm-mount/bin/busybox

	sudo bash -c 'cat > dm-mount/bin/init.sh' <<- EOF
	#!/bin/busybox sh
	busybox echo "System Booted $MSG"
	busybox poweroff -f
	EOF
	sudo chmod +x dm-mount/bin/init.sh
}

create_4part()
{
	# Create disk
	DISK=$1
	PREFIX=$2
	TYPE=${3-snap}
	DM_DEV=dm-${TYPE}
	if [ -f "$DISK" ] && [ -f "${DISK}".info ]; then
		echo "$DISK already exist, not creating it"
		return
	else
		echo "creating $DISK"
	fi

	dd if=/dev/zero of="$DISK" bs=256M count=4
	# Partition 1
	sudo sgdisk -n 1:0:+200M -t 1:7f01 "$DISK"
	# Partition 2
	sudo sgdisk -n 2:0:+200M -t 2:7f01 "$DISK"
	# Partition 3
	sudo sgdisk -n 3:0:+200M -t 3:7f01 "$DISK"
	# Partition 4
	sudo sgdisk -n 4:0:+200M -t 4:7f01 "$DISK"

	LOOP_DEV=$(sudo losetup -fP --show "$DISK")
	sectors=$(sudo blockdev --getsz "${LOOP_DEV}"p1)
	sudo mkfs.ext4 -L ROOT-"${TYPE}"p1 "${LOOP_DEV}"p1
	mkdir -p dm-mount
	sudo mount "${LOOP_DEV}"p1 dm-mount

	fill_rootfs  "${TYPE} disk $DISK p1"

	# Umount disk/dm/loopback
	sudo umount dm-mount
	rm -r dm-mount

	sudo mkfs.ext4 -L ROOT-"${TYPE}"p2 "${LOOP_DEV}"p2
	mkdir -p dm-mount
	sudo mount "${LOOP_DEV}"p2 dm-mount

	fill_rootfs  "${TYPE} disk $DISK p2"

	# Umount disk/dm/loopback
	sudo umount dm-mount
	rm -r dm-mount
	sudo losetup -d "$LOOP_DEV"

	bash -c "cat > ${DISK}.info" <<- EOF
	${PREFIX}sectors=${sectors}
	EOF
}

create_raid()
{
	# Create disk
	DISK=$1
	PREFIX=$2
	TYPE=${3-raid4}
	DM_DEV=dm-raid
	if [ -f "$DISK" ] && [ -f "${DISK}".info ]; then
		echo "$DISK already exist, not creating it"
		return
	else
		echo "creating $DISK"
	fi

	dd if=/dev/zero of="$DISK" bs=256M count=4
	# Partition 1
	sudo sgdisk -n 1:0:+200M -t 1:7f01 "$DISK"
	# Partition 2
	sudo sgdisk -n 2:0:+200M -t 2:7f01 "$DISK"
	# Partition 3
	sudo sgdisk -n 3:0:+200M -t 3:7f01 "$DISK"
	# Partition 4
	sudo sgdisk -n 4:0:+200M -t 4:7f01 "$DISK"

	LOOP_DEV=$(sudo losetup -fP --show "$DISK")

	sectors=$(sudo blockdev --getsz "${LOOP_DEV}"p1)
	sectors=$(($(sudo blockdev --getsz "${LOOP_DEV}"p2) + "$sectors"))
	if [ "$TYPE" != "raid6_zr" ]; then
		sectors=$(($(sudo blockdev --getsz "${LOOP_DEV}"p3) + "$sectors"))
	fi

	table="0 $sectors raid ${TYPE} 3 64 region_size 1024 4 - ${LOOP_DEV}p1 - ${LOOP_DEV}p2 - ${LOOP_DEV}p3 - ${LOOP_DEV}p4"
	echo "$table"
	echo "$table" | sudo dmsetup create $DM_DEV

	echo "concise table (with loopback device)"
	sudo dmsetup table --concise /dev/mapper/$DM_DEV

	# Format
	sudo mkfs.ext4 -L ROOT-RAID /dev/mapper/$DM_DEV
	mkdir -p dm-mount
	sudo mount /dev/mapper/$DM_DEV dm-mount

	fill_rootfs "raid disk $DISK"

	# Umount disk/dm/loopback
	sudo umount dm-mount
	rm -r dm-mount
	sudo dmsetup remove $DM_DEV
	sudo losetup -d "$LOOP_DEV"

	bash -c "cat > ${DISK}.info" <<- EOF
	${PREFIX}sectors=${sectors}
	EOF

}

create_crypt()
{
	# Create disk
	DISK=$1
	PREFIX=$2
	DM_DEV=dm-crypt
	if [ -f "$DISK" ] && [ -f "${DISK}".info ]; then
		echo "$DISK already exist, not creating it"
		return
	else
		echo "creating $DISK"
	fi

	dd if=/dev/zero of="$DISK" bs=256M count=2
	LOOP_DEV=$(sudo losetup -fP --show "$DISK")
	sectors=$(sudo blockdev --getsz "${LOOP_DEV}")
	key=babebabebabebabebabebabebabebabebabebabebabebabebabebabebabebabe
	table="0 $sectors crypt aes-xts-plain64 $key 0 ${LOOP_DEV} 0 1 allow_discards"
	echo "$table" | sudo dmsetup create $DM_DEV

	echo "concise table (with loopback device)"
	sudo dmsetup table --concise /dev/mapper/$DM_DEV

	# Format
	sudo mkfs.ext4 -L ROOT-CRYPT /dev/mapper/$DM_DEV
	mkdir -p dm-mount
	sudo mount /dev/mapper/$DM_DEV dm-mount

	fill_rootfs  "crypt disk $DISK"

	# Umount disk/dm/loopback
	sudo umount dm-mount
	rm -r dm-mount
	sudo dmsetup remove $DM_DEV
	sudo losetup -d "$LOOP_DEV"

	bash -c "cat > ${DISK}.info" <<- EOF
	${PREFIX}sectors=${sectors}
	${PREFIX}key=${key}
	EOF

}

create_striped()
{
	# Create disk
	DISK=$1
	PREFIX=$2
	DM_DEV=dm-striped
	if [ -f "$DISK" ] && [ -f "${DISK}".info ]; then
		echo "$DISK already exist, not creating it"
		return
	else
		echo "creating $DISK"
	fi

	dd if=/dev/zero of="$DISK" bs=256M count=4
	# Partition 1
	sudo sgdisk -n 1:0:+200M -t 1:7f01 "$DISK"
	# Partition 2
	sudo sgdisk -n 2:0:+200M -t 2:7f01 "$DISK"
	# Partition 3
	sudo sgdisk -n 3:0:+200M -t 3:7f01 "$DISK"
	# Partition 4
	sudo sgdisk -n 4:0:+200M -t 4:7f01 "$DISK"

	LOOP_DEV=$(sudo losetup -fP --show "$DISK")

	sectors=$(sudo blockdev --getsz "${LOOP_DEV}"p1)
	sectors=$(($(sudo blockdev --getsz "${LOOP_DEV}"p2) + "$sectors"))
	sectors=$(($(sudo blockdev --getsz "${LOOP_DEV}"p3) + "$sectors"))
	sectors=$(($(sudo blockdev --getsz "${LOOP_DEV}"p4) + "$sectors"))

	PAGESIZE=$(getconf PAGESIZE)

	table="0 $sectors striped 4 $PAGESIZE ${LOOP_DEV}p1 0 ${LOOP_DEV}p2 0 ${LOOP_DEV}p3 0 ${LOOP_DEV}p4 0"
	echo "$table" | sudo dmsetup create $DM_DEV

	echo "concise table (with loopback device)"
	sudo dmsetup table --concise /dev/mapper/$DM_DEV

	# Format
	sudo mkfs.ext4 -L ROOT-STRIPED /dev/mapper/$DM_DEV
	mkdir -p dm-mount
	sudo mount /dev/mapper/$DM_DEV dm-mount

	fill_rootfs  "striped disk $DISK"

	# Umount disk/dm/loopback
	sudo umount dm-mount
	rm -r dm-mount
	sudo dmsetup remove $DM_DEV
	sudo losetup -d "$LOOP_DEV"

	bash -c "cat > ${DISK}.info" <<- EOF
	${PREFIX}sectors=${sectors}
	${PREFIX}PAGESIZE=${PAGESIZE}
	EOF

}

# Create a simple rootfs - target verity
create_verity()
{
	# Create disk
	DISK=$1
	PREFIX=$2
	if [ -f "$DISK" ] && [ -f "${DISK}".info ]; then
		echo "$DISK already exist, not creating it"
		return
	else
		echo "creating $DISK"
	fi

	dd if=/dev/zero of="$DISK" bs=512M count=3
	# Partition 1
	sudo sgdisk -n 1:0:+800M -t 1:7f01 "$DISK"
	# Partition 2
	sudo sgdisk -n 2:0:+500M -t 2:7f01 "$DISK"

	# Format and mount partition 1
	LOOP_DEV=$(sudo losetup -fP --show "$DISK")
	sudo mkfs.ext4 -L ROOT-VERITY "${LOOP_DEV}"p1
	mkdir -p dm-mount
	sudo mount "${LOOP_DEV}"p1 dm-mount

	fill_rootfs  "verity disk $DISK"

	# Umount partition
	sudo umount dm-mount
	rm -r dm-mount

	# Format verity
	tmpfile=$(mktemp)
	# shellcheck disable=SC2024
	sudo veritysetup format "${LOOP_DEV}"p1 "${LOOP_DEV}"p2 > "$tmpfile"
	sed -i -e '1d' "$tmpfile"
	sed -i -e 's/./\L&/g' "$tmpfile"
	sed -i -e 's/[[:space:]]\+/_/g' "$tmpfile"
	sed -i -e "s/:_/=/g" "$tmpfile"
	sectors=$(sudo blockdev --getsz "${LOOP_DEV}"p1)
	echo "sectors=${sectors}" >> "$tmpfile"

	# Add prefix to variables in file
	sed -i -e "s/^/$PREFIX/" "$tmpfile"
	mv "$tmpfile" "${DISK}".info

	# Umount loopback
	sudo losetup -d "$LOOP_DEV"
}

# Create a simple rootfs - not device mapped
create_simple-hd()
{
	# Create 1g disk
	DISK=$1
	PREFIX=$2
	if [ -f "$DISK" ] ; then
		echo "$DISK already exist, not creating it"
		return
	else
		echo "creating $DISK"
	fi

	dd if=/dev/zero of="$DISK" bs=512M count=2

	# Create linear device
	LOOP_DEV=$(sudo losetup -fP --show "$DISK")

	# Format
	sudo mkfs.ext4 -L ROOT-LINEAR "$LOOP_DEV"
	mkdir -p dm-mount
	sudo mount "$LOOP_DEV" dm-mount

	fill_rootfs  "simple-hd disk $DISK"

	# Umount disk/dm/loopback
	sudo umount dm-mount
	rm -r dm-mount
}

# Create a simple rootfs - target linear from 4 joined partitions
create_linear()
{
	# Create 1g disk
	DISK=$1
	PREFIX=$2
	DM_DEV=dm-linear
	if [ -f "$DISK" ] && [ -f "${DISK}".info ]; then
		echo "$DISK already exist, not creating it"
		return
	else
		echo "creating $DISK"
	fi

	dd if=/dev/zero of="$DISK" bs=512M count=2
	# Partition 1 - bootable
	sudo sgdisk -n 1:0:+16M -t 1:7f00 "$DISK"
	# Partition 2
	sudo sgdisk -n 2:0:+500M -t 2:7f01 "$DISK"
	# Partition 3
	sudo sgdisk -n 3:0:+100M -t 3:7f01 "$DISK"
	# Partition 4
	sudo sgdisk -n 4:0:+250M -t 4:7f01 "$DISK"

	# Create linear device
	LOOP_DEV=$(sudo losetup -fP --show "$DISK")
	S1=$(sudo blockdev --getsz "${LOOP_DEV}"p1)
	S2=$(sudo blockdev --getsz "${LOOP_DEV}"p2)
	S3=$(sudo blockdev --getsz "${LOOP_DEV}"p3)
	S4=$(sudo blockdev --getsz "${LOOP_DEV}"p4)

	tmpfile=$(mktemp)
	bash -c "cat > $tmpfile" <<- EOF
	P1_START=0
	P1_SIZE=$S1
	P2_START=$S1
	P2_SIZE=$S2
	P3_START=$((S1 + S2))
	P3_SIZE=$S3
	P4_START=$((S1 + S2 + S3))
	P4_SIZE=$S4
	EOF

	source "$tmpfile"

	# Add prefix to variables in file
	sed -i -e "s/^/$PREFIX/" "$tmpfile"

	table="$P1_START $P1_SIZE linear ${LOOP_DEV}p1 0
	$P2_START $P2_SIZE linear ${LOOP_DEV}p2 0
	$P3_START $P3_SIZE linear ${LOOP_DEV}p3 0
	$P4_START $P4_SIZE linear ${LOOP_DEV}p4 0"
	echo "$table" | sudo dmsetup create $DM_DEV

	echo "concise table (with loopback device)"
	sudo dmsetup table --concise /dev/mapper/$DM_DEV

	# Format
	sudo mkfs.ext4 -L ROOT-LINEAR /dev/mapper/$DM_DEV
	mkdir -p dm-mount
	sudo mount /dev/mapper/$DM_DEV dm-mount

	fill_rootfs  "linear disk $DISK"

	# Umount disk/dm/loopback
	sudo umount dm-mount
	rm -r dm-mount
	sudo dmsetup remove $DM_DEV
	sudo losetup -d "$LOOP_DEV"

	mv "$tmpfile" "${DISK}".info
}

launch_test()
{
	DESC="$1"
	KVM_PARAM="$2"
	ROOT_DEV="$3"
	DM_BOOT_PARAM="$4"
	EXPECT_RESULT="${5-0}"
	LOG_N="${LOG_N-0}"
	N_SUCCESS="${N_SUCCESS-0}"
	N_FAIL="${N_FAIL-0}"

	LOG_N=$((LOG_N+1))

	local args="loglevel=15 nokaslr printk.synchronous=1 console=ttyS0 dm-mod.create=\"${DM_BOOT_PARAM}\" root=${ROOT_DEV} ro init=/bin/init.sh"

	mkdir -p logs/${LOG_N}

	echo ""
	echo "=== Test $LOG_N ($(date)):" | tee logs/${LOG_N}/info
	echo "description:: $DESC" | tee -a logs/${LOG_N}/info
	echo "kernel: $KERNEL" | tee -a logs/${LOG_N}/info
	echo "kvm_param: $KVM_PARAM" | tee -a logs/${LOG_N}/info
	echo "dm_param: $DM_BOOT_PARAM" | tee -a logs/${LOG_N}/info
	echo "root_dev: $ROOT_DEV" | tee -a logs/${LOG_N}/info

	cmd="timeout --foreground 15 qemu-system-x86_64 --enable-kvm ${KVM_PARAM} -smp 1 -m 2g -nographic -kernel ${KERNEL} -append '${args}' > logs/${LOG_N}/qemu 2>&1"
	echo "$cmd" >> logs/${LOG_N}/info
	eval "$cmd" && ret=$? || ret=$?
	if [ "$ret" == "${EXPECT_RESULT}" ]; then
		echo "> Result: SUCCESS ($ret)"
		N_SUCCESS=$((N_SUCCESS+1))
	else
		echo "> Result: FAIL ($ret)"
		N_FAIL=$((N_FAIL+1))
	fi
	echo ""
	echo "Total successes: ${N_SUCCESS}. Total failures: ${N_FAIL}"
}

linear_parser_tests()
{
	launch_test \
	"linear: should fail (test the test)" \
	"-hda ${DL1}" \
	/dev/dm-0 \
	"dm-linear,,0,rw,0 0 linear 8:1 0,0 0 linear 8:2 0,0 0 linear 8:3 0,0 0 linear 8:4 0" \
	124

	launch_test \
	"linear: with minor" \
	"-hda ${DL1}" \
	/dev/dm-4 \
	"dm-linear,,4,rw,$DL1_P1_START $DL1_P1_SIZE linear 8:1 0,$DL1_P2_START $DL1_P2_SIZE linear 8:2 0,$DL1_P3_START $DL1_P3_SIZE linear 8:3 0,$DL1_P4_START $DL1_P4_SIZE linear 8:4 0"

	launch_test \
	"linear: big minor 255" \
	"-hda ${DL1}" \
	/dev/dm-255 \
	"dm-linear,,255,rw,$DL1_P1_START $DL1_P1_SIZE linear 8:1 0,$DL1_P2_START $DL1_P2_SIZE linear 8:2 0,$DL1_P3_START $DL1_P3_SIZE linear 8:3 0,$DL1_P4_START $DL1_P4_SIZE linear 8:4 0"

	launch_test \
	"linear: big minor 256" \
	"-hda ${DL1}" \
	/dev/dm-0 \
	"dm-linear,,256,rw,$DL1_P1_START $DL1_P1_SIZE linear 8:1 0,$DL1_P2_START $DL1_P2_SIZE linear 8:2 0,$DL1_P3_START $DL1_P3_SIZE linear 8:3 0,$DL1_P4_START $DL1_P4_SIZE linear 8:4 0"

	launch_test \
	"linear: big minor 257" \
	"-hda ${DL1}" \
	/dev/dm-1 \
	"dm-linear,,257,rw,$DL1_P1_START $DL1_P1_SIZE linear 8:1 0,$DL1_P2_START $DL1_P2_SIZE linear 8:2 0,$DL1_P3_START $DL1_P3_SIZE linear 8:3 0,$DL1_P4_START $DL1_P4_SIZE linear 8:4 0"

	launch_test \
	"linear: huge minor" \
	"-hda ${DL1}" \
	/dev/dm-123123123123123123123 \
	"dm-linear,,123123123123123123123,rw,$DL1_P1_START $DL1_P1_SIZE linear 8:1 0,$DL1_P2_START $DL1_P2_SIZE linear 8:2 0,$DL1_P3_START $DL1_P3_SIZE linear 8:3 0,$DL1_P4_START $DL1_P4_SIZE linear 8:4 0" \
	124

	UUID=123123123123
	launch_test \
	"linear: with uuid (but booting from /dev/dm-0)" \
	"-hda ${DL1}" \
	"/dev/dm-0" \
	"dm-linear,${UUID},,rw,$DL1_P1_START $DL1_P1_SIZE linear 8:1 0,$DL1_P2_START $DL1_P2_SIZE linear 8:2 0,$DL1_P3_START $DL1_P3_SIZE linear 8:3 0,$DL1_P4_START $DL1_P4_SIZE linear 8:4 0"

	launch_test \
	"linear: with device path" \
	"-hda ${DL1}" \
	/dev/dm-4 \
	"dm-linear,,4,rw,$DL1_P1_START $DL1_P1_SIZE linear /dev/sda1 0,$DL1_P2_START $DL1_P2_SIZE linear /dev/sda2 0,$DL1_P3_START $DL1_P3_SIZE linear /dev/sda3 0,$DL1_P4_START $DL1_P4_SIZE linear /dev/sda4 0"

	launch_test \
	"linear: No minor" \
	"-hda ${DL1}" \
	/dev/dm-0 \
	"dm-linear,,,rw,$DL1_P1_START $DL1_P1_SIZE linear 8:1 0,$DL1_P2_START $DL1_P2_SIZE linear 8:2 0,$DL1_P3_START $DL1_P3_SIZE linear 8:3 0,$DL1_P4_START $DL1_P4_SIZE linear 8:4 0"

	launch_test \
	"linear: minor to 0" \
	"-hda ${DL1}" \
	/dev/dm-0 \
	"dm-linear,,0,rw,$DL1_P1_START $DL1_P1_SIZE linear 8:1 0,$DL1_P2_START $DL1_P2_SIZE linear 8:2 0,$DL1_P3_START $DL1_P3_SIZE linear 8:3 0,$DL1_P4_START $DL1_P4_SIZE linear 8:4 0"

	args="dm-linear1,,4,rw,$DL1_P1_START $DL1_P1_SIZE linear /dev/sda1 0,$DL1_P2_START $DL1_P2_SIZE linear /dev/sda2 0,$DL1_P3_START $DL1_P3_SIZE linear /dev/sda3 0,$DL1_P4_START $DL1_P4_SIZE linear /dev/sda4 0"
	args+=";dm-linear2,,5,rw,$DL2_P1_START $DL2_P1_SIZE linear /dev/sdb1 0,$DL2_P2_START $DL2_P2_SIZE linear /dev/sdb2 0,$DL2_P3_START $DL2_P3_SIZE linear /dev/sdb3 0,$DL2_P4_START $DL2_P4_SIZE linear /dev/sdb4 0"
	launch_test \
	"linear: multiple disks (boot sda)" \
	"-hda ${DL1} -hdb ${DL2} " \
	/dev/dm-4 \
	"$args"

	args="dm-linear1,,4,rw,$DL1_P1_START $DL1_P1_SIZE linear /dev/sda1 0,$DL1_P2_START $DL1_P2_SIZE linear /dev/sda2 0,$DL1_P3_START $DL1_P3_SIZE linear /dev/sda3 0,$DL1_P4_START $DL1_P4_SIZE linear /dev/sda4 0"
	args+=";dm-linear2,,5,rw,$DL2_P1_START $DL2_P1_SIZE linear /dev/sdb1 0,$DL2_P2_START $DL2_P2_SIZE linear /dev/sdb2 0,$DL2_P3_START $DL2_P3_SIZE linear /dev/sdb3 0,$DL2_P4_START $DL2_P4_SIZE linear /dev/sdb4 0"
	launch_test \
	"linear: multiple disks (boot sdb)" \
	"-hda ${DL1} -hdb ${DL2} " \
	/dev/dm-5 \
	"$args"

	args="   dm-linear  ,  ,   100  ,  rw  ,  $DL1_P1_START   $DL1_P1_SIZE   linear   8:1   0  ,  $DL1_P2_START $DL1_P2_SIZE   linear   8:2    0  ,   $DL1_P3_START    $DL1_P3_SIZE    linear    8:3   0   ,    $DL1_P4_START $DL1_P4_SIZE    linear    8:4    0   "
	args+="  ;   dm-linear2 ,   , ,ro,   $DL2_P1_START $DL2_P1_SIZE   linear /dev/sdb1 0,$DL2_P2_START $DL2_P2_SIZE linear /dev/sdb2 0,$DL2_P3_START $DL2_P3_SIZE linear /dev/sdb3 0,$DL2_P4_START $DL2_P4_SIZE linear /dev/sdb4 0   "
	launch_test \
	"linear: spaces" \
	"-hda ${DL1} -hdb ${DL2} " \
	/dev/dm-0 \
	"$args"

	launch_test \
	"linear: name with space" \
	"-hda ${DL1}" \
	/dev/dm-4 \
	"this is a name,,4,rw,$DL1_P1_START $DL1_P1_SIZE linear 8:1 0,$DL1_P2_START $DL1_P2_SIZE linear 8:2 0,$DL1_P3_START $DL1_P3_SIZE linear 8:3 0,$DL1_P4_START $DL1_P4_SIZE linear 8:4 0" \

	launch_test \
	"linear: bad argument: bad name" \
	"-hda ${DL1}" \
	/dev/dm-4 \
	"/dev/asdf,,4,rw,$DL1_P1_START $DL1_P1_SIZE linear 8:1 0,$DL1_P2_START $DL1_P2_SIZE linear 8:2 0,$DL1_P3_START $DL1_P3_SIZE linear 8:3 0,$DL1_P4_START $DL1_P4_SIZE linear 8:4 0" \
	124

	launch_test \
	"linear: bad argument: bad minor" \
	"-hda ${DL1}" \
	/dev/dm-4 \
	"dm-linear,,BAD,rw,$DL1_P1_START $DL1_P1_SIZE linear 8:1 0,$DL1_P2_START $DL1_P2_SIZE linear 8:2 0,$DL1_P3_START $DL1_P3_SIZE linear 8:3 0,$DL1_P4_START $DL1_P4_SIZE linear 8:4 0" \
	124

	launch_test \
	"linear: bad argument: bad read/write" \
	"-hda ${DL1}" \
	/dev/dm-4 \
	"dm-linear,,,BAD,$DL1_P1_START $DL1_P1_SIZE linear 8:1 0,$DL1_P2_START $DL1_P2_SIZE linear 8:2 0,$DL1_P3_START $DL1_P3_SIZE linear 8:3 0,$DL1_P4_START $DL1_P4_SIZE linear 8:4 0" \
	124

	launch_test \
	"linear: bad argument: missing field" \
	"-hda ${DL1}" \
	/dev/dm-4 \
	"dm-linear,,,rw" \
	124

	launch_test \
	"linear: bad argument: missing field in table" \
	"-hda ${DL1}" \
	/dev/dm-4 \
	"dm-linear,,BAD,rw,$DL1_P1_START $DL1_P1_SIZE linear 8:1 0,$DL1_P2_START $DL1_P2_SIZE linear 8:2 0,$DL1_P3_START $DL1_P3_SIZE linear 8:3 0, $DL1_P4_SIZE linear 8:4 0" \
	124

	launch_test \
	"linear: bad argument: empty" \
	"-hda ${DL1}" \
	/dev/dm-4 \
	"" \
	124

	launch_test \
	"linear: bad argument: negative minor number" \
	"-hda ${DL1}" \
	/dev/dm-4 \
	"dm-linear,,-4,ro,$DL1_P1_START $DL1_P1_SIZE linear 8:1 0,$DL1_P2_START $DL1_P2_SIZE linear 8:2 0,$DL1_P3_START $DL1_P3_SIZE linear 8:3 0,$DL1_P4_START $DL1_P4_SIZE linear 8:4 0" \
	124

	launch_test \
	"linear: ending with semi-colon (empty device)" \
	"-hda ${DL1}" \
	/dev/dm-4 \
	"dm-linear,,4,rw,$DL1_P1_START $DL1_P1_SIZE linear /dev/sda1 0,$DL1_P2_START $DL1_P2_SIZE linear /dev/sda2 0,$DL1_P3_START $DL1_P3_SIZE linear /dev/sda3 0,$DL1_P4_START $DL1_P4_SIZE linear /dev/sda4 0;" \
	124

	name="this is a really loooooooooooong name for a deviceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee - reaaaaaaaaaly loooooooooooooooooooooooooong nnnnnnnnnnnnnnnnnnnname"
	name+=" this is a really loooooooooooong name for a deviceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee - reaaaaaaaaaly loooooooooooooooooooooooooong nnnnnnnnnnnnnnnnnnnname"
	name+=" this is a really loooooooooooong name for a deviceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee - reaaaaaaaaaly loooooooooooooooooooooooooong nnnnnnnnnnnnnnnnnnnname"
	name+=" this is a really loooooooooooong name for a deviceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee - reaaaaaaaaaly loooooooooooooooooooooooooong nnnnnnnnnnnnnnnnnnnname"
	name+=" this is a really loooooooooooong name for a deviceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee - reaaaaaaaaaly loooooooooooooooooooooooooong nnnnnnnnnnnnnnnnnnnname"
	name+=" this is a really loooooooooooong name for a deviceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee - reaaaaaaaaaly loooooooooooooooooooooooooong nnnnnnnnnnnnnnnnnnnname"
	name+=" this is a really loooooooooooong name for a deviceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee - reaaaaaaaaaly loooooooooooooooooooooooooong nnnnnnnnnnnnnnnnnnnname"
	name+=" this is a really loooooooooooong name for a deviceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee - reaaaaaaaaaly loooooooooooooooooooooooooong nnnnnnnnnnnnnnnnnnnname"
	name+=" this is a really loooooooooooong name for a deviceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee - reaaaaaaaaaly loooooooooooooooooooooooooong nnnnnnnnnnnnnnnnnnnname"
	name+=" this is a really loooooooooooong name for a deviceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee - reaaaaaaaaaly loooooooooooooooooooooooooong nnnnnnnnnnnnnnnnnnnname"
	name+=" this is a really loooooooooooong name for a deviceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee - reaaaaaaaaaly loooooooooooooooooooooooooong nnnnnnnnnnnnnnnnnnnname"
	launch_test \
	"linear: bad argument: too long name" \
	"-hda ${DL1}" \
	/dev/dm-4 \
	"$name,,4,rw,$DL1_P1_START $DL1_P1_SIZE linear 8:1 0,$DL1_P2_START $DL1_P2_SIZE linear 8:2 0,$DL1_P3_START $DL1_P3_SIZE linear 8:3 0,$DL1_P4_START $DL1_P4_SIZE linear 8:4 0" \
	124
}

verity_tests()
{
	launch_test \
	"verity: with minor" \
	"-hda ${DV1}" \
	/dev/dm-4 \
	"dm-verity,,4,ro,0 $DV1_sectors verity $DV1_hash_type 8:1 8:2 $DV1_data_block_size $DV1_hash_block_size $DV1_data_blocks 1 $DV1_hash_algorithm $DV1_root_hash $DV1_salt"

	launch_test \
	"verity: table fail" \
	"-hda ${DV1} -hdb ${DHD1}" \
	/dev/sdb \
	"dm-verity,,4,ro,0 $DV1_sectors verity payload=ROOT_DEV hashtree=HASH_DEV hashstart=$DV1_sectors alg=$DV1_hash_algorithm root_hexdigest=$DV1_root_hash salt=$DV1_salt"

	UUID=CRYPT-VERITY-543c115f6fb048e2bd7ecbb5960aae4d-verity-test
	launch_test \
	"verity: with uuid" \
	"-hda ${DV1}" \
	/dev/dm-4 \
	"dm-verity,${UUID},4,ro,0 $DV1_sectors verity $DV1_hash_type 8:1 8:2 $DV1_data_block_size $DV1_hash_block_size $DV1_data_blocks 1 $DV1_hash_algorithm $DV1_root_hash $DV1_salt"
}

striped_tests()
{
	launch_test \
	"striped: with minor" \
	"-hda ${DS1}" \
	/dev/dm-4 \
	"dm-striped,,4,ro,0 $DS1_sectors striped 4 $DS1_PAGESIZE 8:1 0 8:2 0 8:3 0 8:4 0"

	UUID=123123123
	launch_test \
	"striped: with uuid" \
	"-hda ${DS1}" \
	/dev/dm-4 \
	"dm-striped,${UUID},4,ro,0 $DS1_sectors striped 4 $DS1_PAGESIZE 8:1 0 8:2 0 8:3 0 8:4 0"
}

crypt_tests()
{
	launch_test \
	"crypt: with minor" \
	"-hda ${DC1}" \
	/dev/dm-4 \
	"dm-crypt,,4,ro,0 $DC1_sectors crypt aes-xts-plain64 $DC1_key 0 /dev/sda 0 1 allow_discards"

	UUID=123123123
	launch_test \
	"crypt: with uuid" \
	"-hda ${DC1}" \
	/dev/dm-4 \
	"dm-crypt,${UUID},4,ro,0 $DC1_sectors crypt aes-xts-plain64 $DC1_key 0 /dev/sda 0 1 allow_discards"

	launch_test \
	"crypt: wrong key" \
	"-hda ${DC1}" \
	/dev/dm-4 \
	"dm-crypt,,4,ro,0 $DC1_sectors crypt aes-xts-plain64 123123123123 0 /dev/sda 0 1 allow_discards" \
	124
}

raid_tests()
{
	# raid is not supported, should fail
	launch_test \
	"raid4: with minor" \
	"-hda ${DR41}" \
	/dev/dm-4 \
	"dm-raid,,4,ro,0 $DR41_sectors raid raid4 3 64 region_size 1024 4 - 8:1 - 8:2 - 8:3 - 8:4" \
	124

	launch_test \
	"raid5: no minor" \
	"-hda ${DR51}" \
	/dev/dm-0 \
	"dm-raid,,,ro,0 $DR51_sectors raid raid5_ls 3 64 region_size 1024 4 - 8:1 - 8:2 - 8:3 - 8:4" \
	124

	launch_test \
	"raid6: no minor" \
	"-hda ${DR61}" \
	/dev/dm-0 \
	"dm-raid,,,ro,0 $DR61_sectors raid raid6_zr 3 64 region_size 1024 4 - 8:1 - 8:2 - 8:3 - 8:4" \
	124
}

snap_tests()
{
	launch_test \
	"snap: snapshot-origin" \
	"-hda ${D4P}" \
	/dev/dm-4 \
	"dm-snap-orig,,4,ro,0 ${D4P_sectors} snapshot-origin 8:1"

	launch_test \
	"snap: snapshot-origin" \
	"-hda ${D4P}" \
	/dev/dm-4 \
	"dm-snap-orig,,4,ro,0 ${D4P_sectors} snapshot-origin 8:2"

	# snapshot and snapshot-merge is not supported, should fail
	launch_test \
	"snap: snapshot" \
	"-hda ${D4P}" \
	/dev/dm-0 \
	"dm-snap,,,ro,0 ${D4P_sectors} snapshot 8:2 8:1 P 8" \
	124

	launch_test \
	"snap: snapshot-merge" \
	"-hda ${D4P}" \
	/dev/dm-0 \
	"dm-snap,,,ro,0 ${D4P_sectors} snapshot-merge 8:1 8:2 P 8" \
	124
}

mirror_tests()
{
	# mirror is not supported, should fail
	launch_test \
	"mirror: core" \
	"-hda ${D4P}" \
	/dev/dm-4 \
	"dm-mirror-core,,4,ro,0 ${D4P_sectors} mirror core 1 1024 2 /dev/sda1 0 /dev/sda2 0 1 handle_errors" \
	124

	launch_test \
	"mirror: core sda3" \
	"-hda ${D4P}" \
	/dev/dm-4 \
	"dm-mirror-core,,4,ro,0 ${D4P_sectors} mirror core 1 1024 2 /dev/sda1 0 /dev/sda3 0 1 handle_errors" \
	124

	launch_test \
	"mirror: disk" \
	"-hda ${D4P}" \
	/dev/dm-4 \
	"dm-mirror-disk,,4,ro,0 ${D4P_sectors} mirror disk 2 /dev/sda3 1024 2 /dev/sda1 0 /dev/sda2 0 1 handle_errors" \
	124
}

cache_tests()
{
	# cache is not supported, should fail
	launch_test \
	"cache: with minor" \
	"-hda ${D4P}" \
	/dev/dm-4 \
	"dm-cache,,4,ro,0 ${D4P_sectors} cache /dev/sda3 /dev/sda2 /dev/sda1 128 0 default 0" \
	124
}

era_tests()
{
	# era is not supported, should fail
	launch_test \
	"era: with minor" \
	"-hda ${D4P}" \
	/dev/dm-4 \
	"dm-era,,4,ro,0 ${D4P_sectors} era /dev/sda1 /dev/sda2 128" \
	124

	launch_test \
	"era: with minor" \
	"-hda ${D4P}" \
	/dev/dm-4 \
	"dm-era,,4,ro,0 ${D4P_sectors} era /dev/sda2 /dev/sda1 128" \
	124

	launch_test \
	"era: with minor" \
	"-hda ${D4P}" \
	/dev/dm-4 \
	"dm-era,,4,ro,0 ${D4P_sectors} era /dev/sda1 /dev/sda2 128" \
	124
}

delay_tests()
{
	launch_test \
	"delay: delay rw 500ms" \
	"-hda ${D4P}" \
	/dev/dm-4 \
	"dm-delay,,4,ro,0 ${D4P_sectors} delay /dev/sda1 0 500"

	launch_test \
	"delay: delay write only 500ms" \
	"-hda ${D4P}" \
	/dev/dm-4 \
	"dm-delay,,4,ro,0 ${D4P_sectors} delay /dev/sda1 0 0 /dev/sda1 0 500"
}

misc_tests()
{
	args="dm-linear1,,1,rw,$DL1_P1_START $DL1_P1_SIZE linear /dev/sda1 0,$DL1_P2_START $DL1_P2_SIZE linear /dev/sda2 0,$DL1_P3_START $DL1_P3_SIZE linear /dev/sda3 0,$DL1_P4_START $DL1_P4_SIZE linear /dev/sda4 0"
	args+=";dm-linear2,,2,rw,$DL2_P1_START $DL2_P1_SIZE linear /dev/sdb1 0,$DL2_P2_START $DL2_P2_SIZE linear /dev/sdb2 0,$DL2_P3_START $DL2_P3_SIZE linear /dev/sdb3 0,$DL2_P4_START $DL2_P4_SIZE linear /dev/sdb4 0"
	args+=";dm-verity,,3,ro,0 $DV1_sectors verity $DV1_hash_type /dev/sdc1 /dev/sdc2 $DV1_data_block_size $DV1_hash_block_size $DV1_data_blocks 1 $DV1_hash_algorithm $DV1_root_hash $DV1_salt"
	args+=";dm-striped,,4,rw,0 $DS1_sectors striped 4 $DS1_PAGESIZE /dev/sdd1 0 /dev/sdd2 0 /dev/sdd3 0 /dev/sdd4 0"
	args+=";dm-crypt,,5,ro,0 $DC1_sectors crypt aes-xts-plain64 $DC1_key 0 /dev/vda 0 1 allow_discards"
	args+=";dm-snap-orig,,6,ro,0 ${D4P_sectors} snapshot-origin /dev/vdb1"
	disks="-hda ${DL1} -hdb ${DL2} -hdc ${DV1} -hdd ${DS1} -drive file=${DC1},if=virtio -drive file=${D4P},if=virtio"

	launch_test \
	"misc: multi devices: boot 1" \
	"$disks" \
	/dev/dm-1 \
	"$args"

	launch_test \
	"misc: multi devices: boot 2" \
	"$disks" \
	/dev/dm-2 \
	"$args"

	launch_test \
	"misc: multi devices: boot 3" \
	"$disks" \
	/dev/dm-3 \
	"$args"

	launch_test \
	"misc: multi devices: boot 4" \
	"$disks" \
	/dev/dm-4 \
	"$args"

	launch_test \
	"misc: multi devices: boot 5" \
	"$disks" \
	/dev/dm-5 \
	"$args"

	launch_test \
	"misc: multi devices: boot 6" \
	"$disks" \
	/dev/dm-6 \
	"$args"
}

DR41=disk-raid4-1.img
create_raid $DR41 DR41_ raid4
# Load disk info
source ${DR41}.info

DR51=disk-raid5-1.img
create_raid $DR51 DR51_ raid5_ls
# Load disk info
source ${DR51}.info

DR61=disk-raid6-1.img
create_raid $DR61 DR61_ raid6_zr
# Load disk info
source ${DR61}.info

DS1=disk-striped-1.img
create_striped $DS1 DS1_
# Load disk info
source ${DS1}.info

DL1=disk-linear-1.img
create_linear $DL1 DL1_
# Load disk info
source ${DL1}.info

DL2=disk-linear-2.img
create_linear $DL2 DL2_
# Load disk info
source ${DL2}.info

DV1=disk-verity-1.img
create_verity $DV1 DV1_
# Load disk info
source ${DV1}.info

DC1=disk-crypt-1.img
create_crypt $DC1 DC1_
# Load disk info
source ${DC1}.info

# used by snap, mirror, cache and era
D4P=disk-4part-1.img
create_4part $D4P D4P_ 4part
# Load disk info
source ${D4P}.info

DHD1=disk-simple-hd-1.img
create_simple-hd $DHD1

verity_tests
linear_parser_tests
striped_tests
crypt_tests
raid_tests
era_tests
snap_tests
mirror_tests
cache_tests
delay_tests
misc_tests
